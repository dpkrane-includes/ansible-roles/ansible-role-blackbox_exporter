# Ansible Prometheus Blackbox Exporter role.

Роль устанавливает последнюю версию Prometheus Blackbox Exporter с GitHub.
 
  ## По умолчанию используются следующие переменные:
blackbox_exporter_user: blackbox_exporter
blackbox_exporter_group: blackbox_exporter
blackbox_exporter_tmp_dir: /tmp
blackbox_exporter_conf_dir: /etc/blackbox
blackbox_exporter_bin: /usr/local/bin/blackbox_exporter
blackbox_exporter_port: 9100

